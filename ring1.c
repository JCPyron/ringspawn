#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define STDIN 0
#define STDOUT 1

int main(int argc, char** argv){
	int fd[2];
	if(argc<2){
		fprintf(stderr, "Err not enough arguments\n");
		exit(2);
	}
	int n = atoi(argv[1]);
	pipe(fd);
	dup2(fd[0],STDIN);
	dup2(fd[1],STDOUT);
	close(fd[0]);
	close(fd[1]);
	int customID = 1;
	int isParent = 0;
	int i;
	for( i = 0; i<n-1 && !isParent; i++){
		pipe(fd);
		isParent = fork();
		if(isParent){
			dup2(fd[1],STDOUT);
		} else{
			customID+=1;
			dup2(fd[0], STDIN);
		}
		close(fd[0]);
		close(fd[1]);
	}

	fprintf(stderr, "%d\n", customID);
	
	

	

	

}
